const papa = require('papaparse')
const fs = require('fs')
const path = require('path')

const convertCsvToJson = () => {            //Helper function to convert csv to json using npm module(papaparse)

    const filePath = '../data/matches.csv'

    const fileContent = fs.readFileSync(filePath, 'utf8')
    const jsonData = papa.parse(fileContent, { header: true })

    return jsonData
}

const matchesPlayedPerYear = (obj) => {         //Function to fetch the matches played per year

    let matchesPerYear = {}

    let year = obj.data[0].season
    let flag = 0;

    //Iterating through each element of the data array
    obj.data.forEach(element => {
        if (element.season === year)
            flag++;
        else {
            matchesPerYear[year] = flag;
            year = element.season;
            flag = 1;
        }
    });

    return matchesPerYear;

}

const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'matchesPerYear_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}


const jsonData = convertCsvToJson();           //Calling the functions 
const result = matchesPlayedPerYear(jsonData)
console.log(isWrittenOrNot(result))
