const papa = require('papaparse')
const fs = require('fs')
const path = require('path')

const convertCsvToJson = () => {            //Helper function to convert csv to json using npm module(papaparse)

    const filePath = '../data/deliveries.csv'

    const fileContent = fs.readFileSync(filePath, 'utf8')
    const jsonData = papa.parse(fileContent, { header: true })

    return jsonData
}

const findBowlersWithSuperOver = (jsonData) => {

    let bowlers = [];
    jsonData.data.forEach(element => {
        if (element.is_super_over > 0) {
            if (!bowlers.includes(element.bowler))
                bowlers.push(element.bowler)
        }
    });
    return bowlers;
}

const economicalSuperBowler = (bowlers, jsonData) => {

    let name = ''
    let count = 0;

    bowlers.forEach(currentBowler => {

        let flag = 0;
        let bowls = 0;
        let runs = 0;
        jsonData.data.forEach(element => {
            if (element.is_super_over > 0 && element.bowler === currentBowler) {
                bowls++;
                runs = runs + element.total_runs;
            }
        });

        flag = runs / bowls;
        if (flag > count) {
            count = flag;
            name = currentBowler;
        }
    });
    return name;
}

const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'superOver_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}
const jsonData = convertCsvToJson()
const bowlers = findBowlersWithSuperOver(jsonData)
const result = economicalSuperBowler(bowlers, jsonData)
console.log(isWrittenOrNot(result))