const papa = require('papaparse')
const fs = require('fs')
const path = require('path')

const convertCsvToJson = () => {            //Helper function to convert csv to json using npm module(papaparse)

    const filePath = '../data/matches.csv'

    const fileContent = fs.readFileSync(filePath, 'utf8')
    const jsonData = papa.parse(fileContent, { header: true })

    return jsonData
}

const wonTossAndTeam = (jsonData) => {

    let numberOfTeamsWon = 0;
    jsonData.data.forEach(element => {

        if (element.toss_winner === element.winner)
            numberOfTeamsWon++;
    });

    return numberOfTeamsWon;
}

const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'teamWonTossAndMatch_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}

const jsonData = convertCsvToJson();
const numberOfTeamsWon = wonTossAndTeam(jsonData)
const result = isWrittenOrNot(numberOfTeamsWon)
console.log(numberOfTeamsWon);