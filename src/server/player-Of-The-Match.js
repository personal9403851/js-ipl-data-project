const papa = require('papaparse')
const fs = require('fs')
const path = require('path')

const convertCsvToJson = () => {            //Helper function to convert csv to json using npm module(papaparse)

    const filePath = '../data/matches.csv'

    const fileContent = fs.readFileSync(filePath, 'utf8')
    const jsonData = papa.parse(fileContent, { header: true })

    return jsonData
}

const playerOfTheMatch = (jsonData) => {

    let playersArray = {};

    let year = jsonData.data[0].season;

    playersArray[year] = [];
    playersArray[year].push(jsonData.data[0].player_of_match);
    jsonData.data.forEach(element => {

        if (element.season === year) {
            if (!playersArray[year].includes(element.player_of_match))
                playersArray[year].push(element.player_of_match)
        }
        else {
            year = element.season;
            playersArray[year] = [];
            playersArray[year].push(element.player_of_match)
        }

    });

    return playersArray
}

const findHighestNumber = (playerOfMatch) => {

    let result = {}

    for (const key in playerOfMatch) {

        let name = '';

        result[key] = [];

        let count = 0;
        playerOfMatch[key].forEach(checkPlayer => {

            let flag = 1;

            playerOfMatch[key].forEach(currentPlayer => {

                if (currentPlayer === checkPlayer)
                    flag++
            });

            if (flag > count) {
                count = flag;
                name = checkPlayer;
            }
        });

        result[key].push(name);
    }

    return result;
}

const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'playerOfTheMatch_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}

const jsonData = convertCsvToJson()
const playerOfMatch = playerOfTheMatch(jsonData)
const result = findHighestNumber(playerOfMatch)
console.log(isWrittenOrNot(result))