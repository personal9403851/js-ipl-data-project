const papa = require('papaparse')
const fs = require('fs')
const path = require('path')
const { match } = require('assert')

const convertCsvToJson = () => {

    const filePathMatches = '../data/matches.csv'

    const fileContentMatches = fs.readFileSync(filePathMatches, 'utf8')
    const jsonDataMatches = papa.parse(fileContentMatches, { header: true })

    const filePathDeliveries = '../data/deliveries.csv'

    const fileContentDeliveries = fs.readFileSync(filePathDeliveries, 'utf8')
    const jsonDataDeliveries = papa.parse(fileContentDeliveries, { header: true })

    const data = [jsonDataMatches, jsonDataDeliveries]

    return data;
}

const findMatchesPlayedIn2015 = (csvToJson) => {

    let match_id = [];

    csvToJson[0].data.forEach(element => {
        if (element.season === '2015')
            match_id.push(element.id)
    });

    return match_id;
}

const findBowlersIn2015 = (csvToJson, match_Id) => {

    let bowlers2015 = [];

    csvToJson[1].data.forEach(element => {

        if (match_Id.includes(element.match_id)) {
            if (!bowlers2015.includes(element.bowler))
                bowlers2015.push(element.bowler)
        }
    });

    return bowlers2015;
}

const findTop10EconomicalBowlers = (csvToJson, bowlers) => {

    let result = {};

    bowlers.forEach(currentBowler => {

        result[currentBowler] = {}

        let economyForCurrentBowler = 0;
        let totalBalls = 0;
        let totalRuns = 0;
        csvToJson[1].data.forEach(element => {
            if (element.bowler === currentBowler) {
                totalRuns += parseInt(element.total_runs);
                totalBalls++;
            }
        });

        let totalOver = totalBalls / 6;
        economyForCurrentBowler = totalRuns / totalOver

        result[currentBowler] = economyForCurrentBowler;
    });

    let resultArray = [];

    for (let key in result) {
        resultArray.push([key, result[key]]);
    }

    const sortedArray = resultArray.sort((a, b) => {
        return a[1] - b[1]
    })

    return sortedArray.slice(0, 10);

}

const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'top10EconomicalBowlers_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}
const csvToJson = convertCsvToJson();
const match_id = findMatchesPlayedIn2015(csvToJson)
const bowlers = findBowlersIn2015(csvToJson, match_id)
const result = findTop10EconomicalBowlers(csvToJson, bowlers)
console.log(isWrittenOrNot(result))