const papa = require('papaparse');
const path = require('path');
const fs = require('fs');

const convertCsvToJson = () => {
    const filePathMatches = '../data/matches.csv';
    const fileContentMatches = fs.readFileSync(filePathMatches, 'utf8');
    const jsonDataMatches = papa.parse(fileContentMatches, { header: true });

    const filePathDeliveries = '../data/deliveries.csv';
    const fileContentDeliveries = fs.readFileSync(filePathDeliveries, 'utf8');
    const jsonDataDeliveries = papa.parse(fileContentDeliveries, { header: true });

    const data = [jsonDataMatches, jsonDataDeliveries];
    return data;
};

const findMatchId = (jsonData) => {
    let match_Id = {};
    let year = jsonData.data[0].season;
    match_Id[year] = [];
    jsonData.data.forEach(element => {
        if (element.season === year) {
            match_Id[year].push(element.id);
        } else {
            year = element.season;
            match_Id[year] = [];
        }
    });
    return match_Id;
};

const findRunsAndBowls = (jsonData, match_Id) => {
    let strike = {};
    let balls = 0;

    let match_Keys = Object.keys(match_Id);

    match_Keys.forEach(year => {
        strike[year] = {};

        jsonData[1].data.forEach(deliveries => {
            if (match_Id[year].includes(deliveries.match_id)) {
                let batsman = deliveries.batsman;

                if (!strike[year][batsman]) {
                    balls = 1;

                    strike[year][batsman] = {};
                    strike[year][batsman]['total_runs'] = 0;
                    strike[year][batsman]['total_balls'] = 0;

                    strike[year][batsman]['total_runs'] = parseInt(deliveries.batsman_runs);
                    strike[year][batsman]['total_balls'] = balls;
                } else {
                    strike[year][batsman]['total_runs'] += parseInt(deliveries.batsman_runs);
                    strike[year][batsman]['total_balls'] += 1;
                }
            }
        });
    });

    return strike;
};

const findStrikeRate = (runsAndBowls) => {
    for (const year in runsAndBowls) {
        for (const player in runsAndBowls[year]) {
            let runs = runsAndBowls[year][player]['total_runs'];
            let balls = runsAndBowls[year][player]['total_balls'];

            // Handle division by zero
            let strikeRate = balls !== 0 ? ((runs / balls) * 100) : 0;

            runsAndBowls[year][player]['strike'] = strikeRate;

            delete (runsAndBowls[year][player]['total_runs'])
            delete (runsAndBowls[year][player]['total_balls'])
        }
    }
    return runsAndBowls
}


const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'strikeRateOfBatsman_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}

const jsonData = convertCsvToJson();
const match_Id = findMatchId(jsonData[0]);
const runsAndBowls = findRunsAndBowls(jsonData, match_Id);
const result = findStrikeRate(runsAndBowls);
console.log(isWrittenOrNot(result));