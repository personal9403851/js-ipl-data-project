const papa = require('papaparse')
const fs = require('fs')
const path = require('path')

const convertCsvToJson = () => {

    const filePath = '../data/matches.csv'

    const fileContent = fs.readFileSync(filePath, 'utf8')
    const jsonData = papa.parse(fileContent, { header: true })

    return jsonData
}

const findNameOfTeams = (obj) => {

    let namesOfTeams = []

    obj.data.forEach(element => {

        let team = element.team1

        if (!namesOfTeams.includes(team))
            namesOfTeams.push(team)

    });

    return namesOfTeams;
}

const matchesWonPerTeam = (namesOfTeams, obj) => {

    let matchesWon = {}

    let year = obj.data[0].season

    namesOfTeams.forEach(team => {

        matchesWon[team] = {}
        let flag = 0;

        obj.data.forEach(element => {


            if (year === element.season) {
                if (team === element.winner) {
                    flag++
                }
            }
            else {

                matchesWon[team][year] = flag;
                flag = 1;
                year = element.season
            }

        });

    });

    return matchesWon;
}

const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'matchesWonPerTeam_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}

const jsonData = convertCsvToJson();
const namesOfTeams = findNameOfTeams(jsonData)
const matches = matchesWonPerTeam(namesOfTeams, jsonData)
const result = isWrittenOrNot(matches)
console.log(result)