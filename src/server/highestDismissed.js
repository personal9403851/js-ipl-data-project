const papa = require('papaparse')
const fs = require('fs')
const path = require('path')

const convertCsvToJson = () => {            //Helper function to convert csv to json using npm module(papaparse)

    const filePath = '../data/deliveries.csv'

    const fileContent = fs.readFileSync(filePath, 'utf8')
    const jsonData = papa.parse(fileContent, { header: true })

    return jsonData
}

const findHighestDismissal = (jsonData) => {

    let count = 0;
    let name = '';
    jsonData.data.forEach(element => {

        if (element.player_dismissed != '') {

            let flag = 0;
            jsonData.data.forEach(element2 => {

                if (element2 === element)
                    flag++
            });
            if (flag > count) {
                count = flag;
                name = element.player_dismissed
            }
        }
    });
    return name;
}

const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'highestDismissed_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}

const jsonData = convertCsvToJson();
const result = findHighestDismissal(jsonData)
console.log(isWrittenOrNot(result))