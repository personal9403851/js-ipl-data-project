const papa = require('papaparse')
const fs = require('fs')
const path = require('path')

const convertCsvToJson = () => {

    const filePathMatches = '../data/matches.csv'

    const fileContentMatches = fs.readFileSync(filePathMatches, 'utf8')
    const jsonDataMatches = papa.parse(fileContentMatches, { header: true })

    const filePathDeliveries = '../data/deliveries.csv'

    const fileContentDeliveries = fs.readFileSync(filePathDeliveries, 'utf8')
    const jsonDataDeliveries = papa.parse(fileContentDeliveries, { header: true })

    const data = [jsonDataMatches, jsonDataDeliveries]

    return data;
}

const findNameOfTeams = (obj) => {

    let namesOfTeams = []

    obj.data.forEach(element => {

        let team = element.team1

        if (!namesOfTeams.includes(team))
            namesOfTeams.push(team)

    });

    return namesOfTeams;
}

const extraRuns = (csvToJson, nameOfTeams) => {

    let jsonMatches = csvToJson[0]
    let jsonDeliveries = csvToJson[1]

    let matchId = [];

    jsonMatches.data.forEach(element => {

        if (element.season === '2016')
            matchId.push(element.id)
    });

    let extraRunsConceded = {};

    nameOfTeams.forEach(element1 => {

        let flag = 0;

        jsonDeliveries.data.forEach(element2 => {

            //console.log(typeof (element2.match_id))
            if (matchId.includes(element2.match_id)) {

                if (element1 === element2.bowling_team) {
                    flag = flag + parseInt(element2.extra_runs);
                }
            }
        });

        extraRunsConceded[element1] = flag;
    });

    return extraRunsConceded;
}

const isWrittenOrNot = (result) => {        //Function to write the output in a JSON file in /src/public folder

    try {

        const fileName = 'extraRunConceded_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}
const csvToJson = convertCsvToJson();
const nameOfTeams = findNameOfTeams(csvToJson[0])
const extra = extraRuns(csvToJson, nameOfTeams)
const result = isWrittenOrNot(extra)
console.log(result)